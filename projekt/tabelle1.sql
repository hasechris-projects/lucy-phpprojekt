-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 29. Mai 2019 um 13:21
-- Server-Version: 10.1.38-MariaDB
-- PHP-Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `projekt`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `tabelle1`
--

CREATE TABLE `tabelle1` (
  `tb_1index` int(10) NOT NULL,
  `tb1_vorname` varchar(20) COLLATE utf8_german2_ci NOT NULL,
  `tb1_nachname` varchar(20) COLLATE utf8_german2_ci NOT NULL,
  `tb1_ladeadresse` varchar(30) COLLATE utf8_german2_ci DEFAULT NULL,
  `tb1_ort` varchar(30) COLLATE utf8_german2_ci DEFAULT NULL,
  `tb1_gewicht` int(6) DEFAULT NULL,
  `tb1_email` varchar(30) COLLATE utf8_german2_ci NOT NULL,
  `tb1_telefon` int(20) DEFAULT NULL,
  `tb1_warenbezeichnung` varchar(50) COLLATE utf8_german2_ci NOT NULL,
  `tb1_datum` varchar(20) COLLATE utf8_german2_ci NOT NULL,
  `tb1_bemerkung` text COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

--
-- Daten für Tabelle `tabelle1`
--

INSERT INTO `tabelle1` (`tb_1index`, `tb1_vorname`, `tb1_nachname`, `tb1_ladeadresse`, `tb1_ort`, `tb1_gewicht`, `tb1_email`, `tb1_telefon`, `tb1_warenbezeichnung`, `tb1_datum`, `tb1_bemerkung`) VALUES
(1, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(2, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(3, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(4, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(5, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(6, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(7, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(8, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(9, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(10, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(11, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(12, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(13, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(14, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(15, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(16, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(17, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(18, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(19, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(20, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'hallo123@gmx.de', 1723463, 'Datensatz', '02.03.2005', 'Hallo, hiermit bewerbe ich mich um einen Platz als Manager in ihrem Bereich.'),
(21, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(22, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(23, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(24, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Hallo', '02.03.2005', 'qwdqwefqwef'),
(25, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Hallo', '02.03.2005', 'qwdqwefqwef'),
(26, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Hallo', '02.03.2005', 'qwdqwefqwef'),
(27, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Hallo', '02.03.2005', 'qwdqwefqwef'),
(28, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Hallo', '02.03.2005', 'qwdqwefqwef'),
(29, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Hallo', '02.03.2005', 'qwdqwefqwef'),
(30, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Hallo', '02.03.2005', 'qwdqwefqwef'),
(31, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Datensatz', '02.03.2005', 'YXP'),
(32, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, 'Datensatz', '02.03.2005', 'YXP'),
(33, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', ''),
(34, 'Bastian', 'Hoppe', 'Lehmgrubenweg 14', 'Leonberg', 71229, 'lucylucyfrank@googlemail.com', 2147483647, '', '', '');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `tabelle1`
--
ALTER TABLE `tabelle1`
  ADD PRIMARY KEY (`tb_1index`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `tabelle1`
--
ALTER TABLE `tabelle1`
  MODIFY `tb_1index` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
